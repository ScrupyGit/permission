package de.chunkworld.permission;

import de.chunkworld.permission.command.CommandRegister;
import de.chunkworld.permission.config.MessageConfig;
import de.chunkworld.permission.database.MongoDB;
import de.chunkworld.permission.database.collection.GroupCollection;
import de.chunkworld.permission.database.collection.PlayerCollection;
import de.chunkworld.permission.database.repository.GroupRepository;
import de.chunkworld.permission.database.repository.GroupRepositoryImpl;
import de.chunkworld.permission.database.repository.PlayerRepository;
import de.chunkworld.permission.database.repository.PlayerRepositoryImpl;
import de.chunkworld.permission.group.GroupLoader;
import de.chunkworld.permission.player.*;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Permission extends JavaPlugin {
  private Prefix prefix;
  private MessageConfig messageConfig;
  private MongoDB mongoDB;
  private GroupCollection groupCollection;
  private PlayerCollection playerCollection;
  private GroupLoader groupLoader;
  private GroupRepository groupRepository;
  private PlayerRepository playerRepository;
  private CommandRegister commandRegister;
  private PlayerGroupLoad playerGroupLoad;
  private PlayerScoreboard playerScoreboard;
  private PlayerPermission playerPermission;

  @Override
  public void onEnable() {
    saveResource("config.yml", false);

    this.prefix = loadPrefix();

    this.messageConfig = MessageConfig.create(getConfig(), this.prefix);

    this.mongoDB = MongoDB.connect("localhost", "27017", "admin");

    this.groupCollection = GroupCollection.create("groups", mongoDB);
    this.groupLoader = GroupLoader.create(groupCollection);
    this.groupRepository = GroupRepositoryImpl.create(groupCollection, groupLoader);
    this.playerCollection= PlayerCollection.create("players", mongoDB);
    this.playerRepository = PlayerRepositoryImpl.create(playerCollection, groupRepository);
    this.commandRegister = CommandRegister.create(this, groupRepository, messageConfig, playerRepository);
    this.playerGroupLoad = PlayerGroupLoad.create(playerRepository);
    this.playerScoreboard = PlayerScoreboard.create(playerRepository, groupRepository);
    this.playerPermission = PlayerPermission.create(this);

    PluginManager pluginManager = Bukkit.getPluginManager();
    pluginManager.registerEvents(playerGroupLoad, this);
    pluginManager.registerEvents(ScoreboardCreater.create(playerScoreboard), this);
    pluginManager.registerEvents(ScoreboardUpdater.create(playerScoreboard), this);
    pluginManager.registerEvents(PermissionLoad.create(playerPermission, playerRepository), this);
    pluginManager.registerEvents(PermissionReload.create(playerPermission, playerRepository), this);

    String pluginEnabledMessage = messageConfig.messageFromKey("pluginEnabled");
    Bukkit.getConsoleSender().sendMessage(pluginEnabledMessage);
  }

  @Override
  public void onDisable() {
    String pluginDisabledMessage = messageConfig.messageFromKey("pluginDisabled");
    Bukkit.getConsoleSender().sendMessage(pluginDisabledMessage);
  }

  private Prefix loadPrefix() {
    String prefixPath = "messages.prefix";
    String prefixMessage = getConfig().getString(prefixPath);
    return Prefix.of(prefixMessage);
  }
}
