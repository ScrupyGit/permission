package de.chunkworld.permission.player;

import com.google.common.base.Preconditions;
import de.chunkworld.permission.group.Group;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;
import java.util.Map;

public final class PlayerPermission {
  private final Plugin plugin;
  private static Map<String, PermissionAttachment> playerAttachments;

  private PlayerPermission(Plugin plugin) {
    this.plugin = plugin;
    playerAttachments = new HashMap<>();
  }

  public void apply(Player player, Group group) {
    PermissionAttachment permissionAttachment = player.addAttachment(plugin);
    for (String permission : group.getPermissions()) {
      permissionAttachment.setPermission(permission, true);
    }
    playerAttachments.put(player.getUniqueId().toString(), permissionAttachment);
  }

  public void clear(Player player) {
    String uuid = player.getUniqueId().toString();
    if (playerAttachments.get(uuid) != null) {
      PermissionAttachment playerPermissions = playerAttachments.get(uuid);
      playerPermissions.remove();
    }
  }

  public static PlayerPermission create(Plugin plugin) {
    Preconditions.checkNotNull(plugin);
    return new PlayerPermission(plugin);
  }
}
