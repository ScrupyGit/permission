package de.chunkworld.permission.player;

import com.google.common.base.Preconditions;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public final class ScoreboardCreater implements Listener {
  private final PlayerScoreboard playerScoreboard;

  private ScoreboardCreater(PlayerScoreboard playerScoreboard) {
    this.playerScoreboard = playerScoreboard;
  }

  @EventHandler
  public void setScoreboard(PlayerJoinEvent join) {
    Player player = join.getPlayer();
    playerScoreboard.apply(player);
  }

  public static ScoreboardCreater create(PlayerScoreboard playerScoreboard) {
    Preconditions.checkNotNull(playerScoreboard);
    return new ScoreboardCreater(playerScoreboard);
  }
}
