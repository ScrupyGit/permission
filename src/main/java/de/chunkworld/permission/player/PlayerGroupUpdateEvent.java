package de.chunkworld.permission.player;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PlayerGroupUpdateEvent extends Event {
  private static final HandlerList HANDLER_LIST = new HandlerList();
  private final String uuid;

  private PlayerGroupUpdateEvent(String uuid) {
    this.uuid = uuid;
  }

  @Override
  public HandlerList getHandlers() {
    return HANDLER_LIST;
  }

  public static HandlerList getHandlerList() {
    return HANDLER_LIST;
  }

  public String getUuid() {
    return uuid;
  }

  public static PlayerGroupUpdateEvent create(String uuid) {
    return new PlayerGroupUpdateEvent(uuid);
  }
}
