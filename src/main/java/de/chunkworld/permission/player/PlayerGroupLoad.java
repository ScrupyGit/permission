package de.chunkworld.permission.player;

import com.google.common.base.Preconditions;
import de.chunkworld.permission.database.repository.PlayerRepository;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;

import java.util.concurrent.ExecutionException;

public final class PlayerGroupLoad implements Listener {
  private final PlayerRepository playerRepository;

  private PlayerGroupLoad(PlayerRepository playerRepository) {
    this.playerRepository = playerRepository;
  }

  @EventHandler
  public void loadRank(AsyncPlayerPreLoginEvent preLogin) {
    String uuid = preLogin.getUniqueId().toString();
    try {
      Boolean hasGroup = playerRepository.hasGroup(uuid).get();
      if (hasGroup) {
        loadGroup(uuid);
      } else {
        insertUUID(uuid);
      }
    } catch (InterruptedException | ExecutionException e) {
      e.printStackTrace();
    }
  }

  private void insertUUID(String uuid) {
    playerRepository.insertUUID(uuid);
  }

  private void loadGroup(String uuid) throws InterruptedException, ExecutionException {
    playerRepository.loadGroup(uuid).get();
  }

  public static PlayerGroupLoad create(PlayerRepository playerRepository) {
    Preconditions.checkNotNull(playerRepository);
    return new PlayerGroupLoad(playerRepository);
  }
}
