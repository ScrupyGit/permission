package de.chunkworld.permission.player;

import com.google.common.base.Preconditions;
import de.chunkworld.permission.database.repository.GroupRepository;
import de.chunkworld.permission.database.repository.PlayerRepository;
import de.chunkworld.permission.group.Group;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

public final class PlayerScoreboard {
  private final PlayerRepository playerRepository;
  private final GroupRepository groupRepository;

  private PlayerScoreboard(PlayerRepository playerRepository, GroupRepository groupRepository) {
    this.playerRepository = playerRepository;
    this.groupRepository = groupRepository;
  }

  public void apply(Player player) {
    Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
    Objective objective = scoreboard.registerNewObjective("Community", "dummy");
    objective.setDisplaySlot(DisplaySlot.SIDEBAR);
    objective.setDisplayName("Community");

    registerGroups(scoreboard);

    PlayerGroup playerGroup = playerRepository.getPlayerGroup(player);
    Group group = playerGroup.getGroup();

    Team team = scoreboard.getTeam(group.getSortId() + group.getName());
    team.addEntry(player.getName());

    player.setScoreboard(scoreboard);
    updateAllScoreboards(player, group);
  }

  private void updateAllScoreboards(Player player, Group group) {
    for (Player all : Bukkit.getOnlinePlayers()) {
      if (all != player) {
        all.getScoreboard().getTeam(group.getSortId() + group.getName()).addEntry(player.getName());

        PlayerGroup playerGroup = playerRepository.getPlayerGroup(all);
        Group iterateGroup = playerGroup.getGroup();

        player.getScoreboard().getTeam(iterateGroup.getSortId() + iterateGroup.getName()).addEntry(all.getName());
      }
    }
  }

  private void registerGroups(Scoreboard scoreboard) {
    for (Group group : groupRepository.getGroups()) {
      scoreboard.registerNewTeam(group.getSortId() + group.getName());
      Team team = scoreboard.getTeam(group.getSortId() + group.getName());
      team.setColor(ChatColor.GRAY);
      team.setPrefix(group.getPrefix().replace("&", "§") + " ");
    }
  }

  public static PlayerScoreboard create(PlayerRepository playerRepository, GroupRepository groupRepository) {
    Preconditions.checkNotNull(playerRepository);
    Preconditions.checkNotNull(groupRepository);
    return new PlayerScoreboard(playerRepository, groupRepository);
  }
}
