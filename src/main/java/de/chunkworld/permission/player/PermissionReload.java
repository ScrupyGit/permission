package de.chunkworld.permission.player;

import com.google.common.base.Preconditions;
import de.chunkworld.permission.database.repository.PlayerRepository;
import de.chunkworld.permission.group.Group;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.UUID;

public class PermissionReload implements Listener {
  private final PlayerPermission playerPermission;
  private final PlayerRepository playerRepository;

  private PermissionReload(
    PlayerPermission playerPermission,
    PlayerRepository playerRepository
  ) {
    this.playerPermission = playerPermission;
    this.playerRepository = playerRepository;
  }

  @EventHandler
  public void updatePermissionOnPlayerUpdate(PlayerGroupUpdateEvent playerUpdate) {
    Player player = Bukkit.getPlayer(UUID.fromString(playerUpdate.getUuid()));
    if (player != null && player.isOnline()) {
      playerPermission.clear(player);
      Group playerGroup = playerRepository.getPlayerGroup(player).getGroup();
      playerPermission.apply(player, playerGroup);
    }
  }

  public static PermissionReload create(
    PlayerPermission playerPermission,
    PlayerRepository playerRepository
  ) {
    Preconditions.checkNotNull(playerPermission);
    Preconditions.checkNotNull(playerRepository);
    return new PermissionReload(playerPermission, playerRepository);
  }
}
