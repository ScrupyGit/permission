package de.chunkworld.permission.player;

import com.google.common.base.Preconditions;
import de.chunkworld.permission.Constants;
import de.chunkworld.permission.group.Group;
import org.bson.Document;

public class PlayerGroup {
  private final String uuid;
  private final Group group;

  private PlayerGroup(String uuid, Group group) {
    this.uuid = uuid;
    this.group = group;
  }

  public Group getGroup() {
    return group;
  }

  public String getUuid() {
    return uuid;
  }

  public Document toDocument() {
    return new Document(Constants.PLAYER_UUID_DOCUMENT_KEY, uuid)
      .append(Constants.PLAYER_GROUP_DOCUMENT_KEY, group.getName());
  }

  public static PlayerGroup create(String uuid, Group group) {
    Preconditions.checkNotNull(group);
    Preconditions.checkNotNull(uuid);
    return new PlayerGroup(uuid, group);
  }
}
