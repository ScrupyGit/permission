package de.chunkworld.permission.player;

import com.google.common.base.Preconditions;
import de.chunkworld.permission.group.GroupUpdateEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public final class ScoreboardUpdater implements Listener {
  private final PlayerScoreboard playerScoreboard;

  private ScoreboardUpdater(PlayerScoreboard playerScoreboard) {
    this.playerScoreboard = playerScoreboard;
  }

  @EventHandler
  public void updateScoreboardOnGroupUpdate(GroupUpdateEvent groupUpdate) {
    updateScoreboardForAllPlayers();
  }

  @EventHandler
  public void updateScoreboardOnPlayerUpdate(PlayerGroupUpdateEvent playerUpdate) {
    updateScoreboardForAllPlayers();
  }

  private void updateScoreboardForAllPlayers() {
    for (Player all : Bukkit.getOnlinePlayers()) {
      playerScoreboard.apply(all);
    }
  }

  public static ScoreboardUpdater create(PlayerScoreboard playerScoreboard) {
    Preconditions.checkNotNull(playerScoreboard);
    return new ScoreboardUpdater(playerScoreboard);
  }
}
