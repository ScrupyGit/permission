package de.chunkworld.permission.player;

import com.google.common.base.Preconditions;
import de.chunkworld.permission.database.repository.PlayerRepository;
import de.chunkworld.permission.group.Group;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public final class PermissionLoad implements Listener {
  private final PlayerPermission playerPermission;
  private final PlayerRepository playerRepository;

  private PermissionLoad(
    PlayerPermission playerPermission,
    PlayerRepository playerRepository
  ) {
    this.playerPermission = playerPermission;
    this.playerRepository = playerRepository;
  }

  @EventHandler
  public void loadPermission(PlayerJoinEvent join) {
    Player player = join.getPlayer();
    Group playerGroup = playerRepository.getPlayerGroup(player).getGroup();
    playerPermission.apply(player, playerGroup);
  }

  public static PermissionLoad create(
    PlayerPermission playerPermission,
    PlayerRepository playerRepository
  ) {
    Preconditions.checkNotNull(playerPermission);
    Preconditions.checkNotNull(playerRepository);
    return new PermissionLoad(playerPermission, playerRepository);
  }
}
