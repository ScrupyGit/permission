package de.chunkworld.permission.database.repository;

import com.google.common.base.Preconditions;
import com.mongodb.async.SingleResultCallback;
import com.mongodb.async.client.MongoCollection;
import com.mongodb.client.model.Filters;

import com.mongodb.client.result.UpdateResult;
import de.chunkworld.permission.Constants;
import de.chunkworld.permission.database.collection.DataCollection;
import de.chunkworld.permission.group.Group;

import de.chunkworld.permission.group.GroupLoader;
import de.chunkworld.permission.group.GroupUpdateEvent;
import de.chunkworld.permission.player.PlayerScoreboard;
import org.bson.Document;

import org.bson.conversions.Bson;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import static de.chunkworld.permission.Constants.DEFAULT_GROUP_NAME;

public class GroupRepositoryImpl implements GroupRepository {
  private final MongoCollection<Document> groupCollection;
  private final GroupLoader groupLoader;
  private Map<String, Group> groups;

  private GroupRepositoryImpl(DataCollection groupCollection, GroupLoader groupLoader) {
    this.groupCollection = groupCollection.getCollection();
    this.groupLoader = groupLoader;
    groupLoader.loadAllGroups().thenAccept(loadedGroups -> {
      this.groups = loadedGroups;
      createDefaultGroupIfNotExists();
    });
  }

  @Override
  public CompletableFuture<Group> addGroup(String groupName, String prefix, int sortId) {
    CompletableFuture<Group> completable = new CompletableFuture<>();
    Group newGroup = Group.create(groupName, prefix, sortId);
    Document groupDocument = newGroup.toDocument();

    groupCollection.insertOne(groupDocument, (aVoid, throwable) -> {
      if (throwable == null) {
        groups.put(newGroup.getName().toLowerCase(), newGroup);
        completable.complete(newGroup);
      } else {
        completable.completeExceptionally(throwable);
      }
    });

    return completable;
  }

  @Override
  public CompletableFuture<Group> addGroup(Group group) {
    CompletableFuture<Group> completable = new CompletableFuture<>();
    Document groupDocument = group.toDocument();

    groupCollection.insertOne(groupDocument, (aVoid, throwable) -> {
      if (throwable == null) {
        groups.put(group.getName().toLowerCase(), group);
        completable.complete(group);
      } else {
        completable.completeExceptionally(throwable);
      }
    });

    return completable;
  }

  @Override
  public CompletableFuture<Boolean> removeGroup(String groupName) {
    CompletableFuture<Boolean> completable = new CompletableFuture<>();

    Group group = groups.get(groupName.toLowerCase());
    Bson deleteFilter = Filters.eq(Constants.GROUP_NAME_DOCUMENT_KEY, group.getName());

    groupCollection.deleteOne(deleteFilter, (deleteResult, throwable) -> {
      if (throwable == null) {
        groups.remove(groupName.toLowerCase());
        completable.complete(true);
      } else {
        completable.completeExceptionally(throwable);
      }
    });

    return completable;
  }

  @Override
  public CompletableFuture<Group> setSortId(String groupName, int sortId) {
    Group group = groups.get(groupName.toLowerCase());
    group.setSortId(sortId);
    return updateGroup(group);
  }

  @Override
  public CompletableFuture<Group> setPrefix(String groupName, String prefix) {
    Group group = groups.get(groupName.toLowerCase());
    group.setPrefix(prefix);
    return updateGroup(group);
  }

  @Override
  public CompletableFuture<Group> addPermission(String groupName, String permission) {
    Group group = groups.get(groupName.toLowerCase());
    group.addPermission(permission);
    return updateGroup(group);
  }

  @Override
  public CompletableFuture<Group> removePermission(String groupName, String permission) {
    Group group = groups.get(groupName.toLowerCase());
    group.removePermission(permission);
    return updateGroup(group);
  }

  @Override
  public CompletableFuture<Group> setGroup(String groupName, Player player) {
    return null;
  }

  @Override
  public CompletableFuture<Group> updateGroup(Group group) {
    CompletableFuture<Group> completable = new CompletableFuture<>();
    Bson replaceFilter = Filters.eq(Constants.GROUP_NAME_DOCUMENT_KEY, group.getName());

    groupCollection.replaceOne(replaceFilter, group.toDocument(), new SingleResultCallback<UpdateResult>() {
      @Override
      public void onResult(UpdateResult updateResult, Throwable throwable) {
        if (throwable == null) {
          completable.complete(group);
        } else {
          completable.completeExceptionally(throwable);
        }
      }
    });
    GroupUpdateEvent groupUpdateEvent = GroupUpdateEvent.create();
    Bukkit.getPluginManager().callEvent(groupUpdateEvent);
    return completable;
  }

  @Override
  public boolean groupExists(String groupName) {
    return groups.containsKey(groupName.toLowerCase());
  }

  @Override
  public Group getGroupByName(String groupName) {
    return groups.get(groupName.toLowerCase());
  }

  @Override
  public void createDefaultGroupIfNotExists() {
    if (!groups.containsKey(DEFAULT_GROUP_NAME)) {
      Group defaultGroup = Group.create(DEFAULT_GROUP_NAME, "&a&lSpieler", 0);
      groups.put(DEFAULT_GROUP_NAME, defaultGroup);
      addGroup(defaultGroup);
    }
  }

  public Collection<Group> getGroups() {
    return groups.values();
  }

  public static GroupRepositoryImpl create(
    DataCollection groupCollection,
    GroupLoader groupLoader
  ) {
    Preconditions.checkNotNull(groupCollection);
    Preconditions.checkNotNull(groupLoader);
    return new GroupRepositoryImpl(groupCollection, groupLoader);
  }
}
