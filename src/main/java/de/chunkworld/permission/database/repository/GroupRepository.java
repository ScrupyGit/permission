package de.chunkworld.permission.database.repository;

import de.chunkworld.permission.group.Group;
import org.bukkit.entity.Player;

import java.util.Collection;
import java.util.concurrent.CompletableFuture;

public interface GroupRepository {
  CompletableFuture<Group> addGroup(String groupName, String prefix, int sortId);
  CompletableFuture<Group> addGroup(Group group);
  CompletableFuture<Boolean> removeGroup(String groupName);
  CompletableFuture<Group> setSortId(String groupName, int sortId);
  CompletableFuture<Group> setPrefix(String groupName, String prefix);
  CompletableFuture<Group> addPermission(String groupName, String permission);
  CompletableFuture<Group> removePermission(String groupName, String permission);
  CompletableFuture<Group> setGroup(String groupName, Player player);
  CompletableFuture<Group> updateGroup(Group group);
  boolean groupExists(String groupName);
  Group getGroupByName(String groupName);
  void createDefaultGroupIfNotExists();
  Collection<Group> getGroups();
}
