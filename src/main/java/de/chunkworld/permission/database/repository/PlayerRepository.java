package de.chunkworld.permission.database.repository;

import de.chunkworld.permission.group.Group;
import de.chunkworld.permission.player.PlayerGroup;
import org.bukkit.entity.Player;

import java.util.concurrent.CompletableFuture;

public interface PlayerRepository {
  CompletableFuture<PlayerGroup> setGroup(String uuid, Group group);
  CompletableFuture<PlayerGroup> insertUUID(String uuid);
  CompletableFuture<Group> loadGroup(String uuid);
  CompletableFuture<Boolean> hasGroup(String uuid);
  CompletableFuture<PlayerGroup> updateUUID(String uuid);
  PlayerGroup getPlayerGroup(Player player);
}
