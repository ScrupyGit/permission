package de.chunkworld.permission.database.repository;

import com.google.common.base.Preconditions;
import com.mongodb.async.SingleResultCallback;
import com.mongodb.async.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.mongodb.client.result.UpdateResult;
import de.chunkworld.permission.database.collection.DataCollection;
import de.chunkworld.permission.group.Group;
import de.chunkworld.permission.group.GroupUpdateEvent;
import de.chunkworld.permission.player.PlayerGroup;
import de.chunkworld.permission.player.PlayerGroupUpdateEvent;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import static de.chunkworld.permission.Constants.*;

public class PlayerRepositoryImpl implements PlayerRepository {
  private final MongoCollection<Document> playerCollection;
  private final Map<String, PlayerGroup> playerGroups;
  private final GroupRepository groupRepository;

  private PlayerRepositoryImpl(DataCollection playerCollection, GroupRepository groupRepository) {
    this.playerCollection = playerCollection.getCollection();
    this.playerGroups = new HashMap<>();
    this.groupRepository = groupRepository;
  }

  @Override
  public CompletableFuture<PlayerGroup> setGroup(String uuid, Group group) {
    PlayerGroup playerGroup = PlayerGroup.create(uuid, group);
    playerGroups.put(uuid, playerGroup);
    Bukkit.getPluginManager().callEvent(PlayerGroupUpdateEvent.create(uuid));
    return updateUUID(uuid);
  }

  @Override
  public CompletableFuture<PlayerGroup> insertUUID(String uuid) {
    CompletableFuture<PlayerGroup> completable = new CompletableFuture<>();
    Group defaultGroup = groupRepository.getGroupByName(DEFAULT_GROUP_NAME);
    if (defaultGroup == null) {
      groupRepository.createDefaultGroupIfNotExists();
    }
    PlayerGroup playerGroup = PlayerGroup.create(uuid, defaultGroup);
    playerGroups.put(uuid, playerGroup);
    playerCollection.insertOne(playerGroup.toDocument(), new SingleResultCallback<Void>() {
      @Override
      public void onResult(Void aVoid, Throwable throwable) {
        if (throwable == null) {
          completable.complete(playerGroup);
        } else {
          completable.completeExceptionally(throwable);
        }
      }
    });
    return completable;
  }

  @Override
  public CompletableFuture<Group> loadGroup(String uuid) {
    CompletableFuture<Group> completable = new CompletableFuture<>();
    Bson findFilter = Filters.eq(PLAYER_UUID_DOCUMENT_KEY, uuid);
    playerCollection.find(findFilter).first(new SingleResultCallback<Document>() {
      @Override
      public void onResult(Document document, Throwable throwable) {
        if (throwable == null) {
          String groupName = document.getString(PLAYER_GROUP_DOCUMENT_KEY);
          Group group = groupRepository.getGroupByName(groupName);
          PlayerGroup playerGroup = PlayerGroup.create(uuid, group);
          playerGroups.put(uuid, playerGroup);
          completable.complete(group);
        } else {
          completable.completeExceptionally(throwable);
        }
      }
    });
    return completable;
  }

  @Override
  public CompletableFuture<Boolean> hasGroup(String uuid) {
    CompletableFuture<Boolean> completable = new CompletableFuture<>();
    Bson findFilter = Filters.eq(PLAYER_UUID_DOCUMENT_KEY, uuid);
    playerCollection.find(findFilter).first(new SingleResultCallback<Document>() {
      @Override
      public void onResult(Document document, Throwable throwable) {
        if (document == null) {
          completable.complete(false);
        } else {
          completable.complete(true);
        }
      }
    });
    return completable;
  }

  @Override
  public CompletableFuture<PlayerGroup> updateUUID(String uuid) {
    CompletableFuture<PlayerGroup> completable = new CompletableFuture<>();
    PlayerGroup playerGroup = playerGroups.get(uuid);
    if (playerGroup != null) {
      Bson updateFilter = Filters.eq(PLAYER_UUID_DOCUMENT_KEY, uuid);
      playerCollection.replaceOne(updateFilter, playerGroup.toDocument(), new SingleResultCallback<UpdateResult>() {
        @Override
        public void onResult(UpdateResult updateResult, Throwable throwable) {
          if (throwable == null) {
            completable.complete(playerGroup);
          } else {
            completable.completeExceptionally(throwable);
          }
        }
      });
    }
    return completable;
  }

  @Override
  public PlayerGroup getPlayerGroup(Player player) {
    return playerGroups.get(player.getUniqueId().toString());
  }

  public static PlayerRepositoryImpl create(DataCollection playerCollection, GroupRepository groupRepository) {
    Preconditions.checkNotNull(playerCollection);
    Preconditions.checkNotNull(groupRepository);
    return new PlayerRepositoryImpl(playerCollection, groupRepository);
  }
}
