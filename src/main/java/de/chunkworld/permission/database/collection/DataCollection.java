package de.chunkworld.permission.database.collection;

import com.mongodb.async.client.MongoCollection;
import org.bson.Document;

public interface DataCollection {
  MongoCollection<Document> getCollection();
}
