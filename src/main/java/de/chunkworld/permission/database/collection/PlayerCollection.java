package de.chunkworld.permission.database.collection;

import com.google.common.base.Preconditions;
import com.mongodb.async.client.MongoCollection;
import de.chunkworld.permission.database.MongoDB;
import org.bson.Document;

public class PlayerCollection implements DataCollection {
  private final MongoCollection<Document> playerCollection;

  private PlayerCollection(String collectionName, MongoDB mongoDB) {
    this.playerCollection = mongoDB.getCollection(collectionName);
  }

  @Override
  public MongoCollection<Document> getCollection() {
    return playerCollection;
  }

  public static PlayerCollection create(String collectionName, MongoDB mongoDB) {
    Preconditions.checkNotNull(collectionName);
    Preconditions.checkNotNull(mongoDB);
    return new PlayerCollection(collectionName, mongoDB);
  }
}
