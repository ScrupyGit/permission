package de.chunkworld.permission.database.collection;

import com.google.common.base.Preconditions;

import com.mongodb.async.client.MongoCollection;

import de.chunkworld.permission.database.MongoDB;
import org.bson.Document;

public final class GroupCollection implements DataCollection {
  private final MongoCollection<Document> groupCollection;

  private GroupCollection(String collectionName, MongoDB mongoDB) {
    this.groupCollection = mongoDB.getCollection(collectionName);
  }

  @Override
  public MongoCollection<Document> getCollection() {
    return groupCollection;
  }

  public static GroupCollection create(String collectionName, MongoDB mongoDB) {
    Preconditions.checkNotNull(collectionName);
    Preconditions.checkNotNull(mongoDB);
    return new GroupCollection(collectionName, mongoDB);
  }
}
