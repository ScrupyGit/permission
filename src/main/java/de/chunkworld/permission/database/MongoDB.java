package de.chunkworld.permission.database;

import com.google.common.base.Preconditions;

import com.mongodb.ConnectionString;
import com.mongodb.async.client.MongoClient;
import com.mongodb.async.client.MongoClients;
import com.mongodb.async.client.MongoCollection;
import com.mongodb.async.client.MongoDatabase;
import org.bson.Document;

import java.text.MessageFormat;

public final class MongoDB {
  private final String hostname;
  private final String port;
  private final MongoClient client;
  private final MongoDatabase database;

  private MongoDB(String hostname, String port, String database) {
    this.hostname = hostname;
    this.port = port;

    String connectionString = MessageFormat.format(
      "mongodb://{0}:{1}", hostname, port);

    this.client = MongoClients.create(new ConnectionString(connectionString));
    this.database = client.getDatabase(database);
  }

  private MongoDB(
    String hostname,
    String port,
    String username,
    String password,
    String database
  ) {
    this.hostname = hostname;
    this.port = port;

    String connectionString = MessageFormat.format(
      "mongodb://{0}:{1}@{2}:{3}/{4}",
      username,
      password,
      hostname,
      port,
      database
    );

    this.client = MongoClients.create(new ConnectionString(connectionString));
    this.database = client.getDatabase(database);
  }

  public MongoCollection<Document> getCollection(String collectionName) {
    return database.getCollection(collectionName);
  }

  public static MongoDB connect(String hostname, String port, String database) {
    Preconditions.checkNotNull(hostname);
    Preconditions.checkNotNull(port);
    Preconditions.checkNotNull(database);
    return new MongoDB(hostname, port, database);
  }

  public static MongoDB connectWithAuthentication(
    String hostname,
    String port,
    String username,
    String password,
    String database
  ) {
    Preconditions.checkNotNull(hostname);
    Preconditions.checkNotNull(port);
    Preconditions.checkNotNull(username);
    Preconditions.checkNotNull(password);
    Preconditions.checkNotNull(database);
    return new MongoDB(hostname, port, username, password, database);
  }
}
