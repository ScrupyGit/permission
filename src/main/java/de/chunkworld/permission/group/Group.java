package de.chunkworld.permission.group;

import com.google.common.base.Preconditions;
import de.chunkworld.permission.Constants;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

public final class Group {
  private String name;
  private String prefix;
  private int sortId;
  private List<String> permissions;

  private Group(String name, String prefix, int sortId) {
    this.name = name;
    this.prefix = prefix;
    this.sortId = sortId;
    this.permissions = new ArrayList<>();
  }

  private Group(String name, String prefix, int sortId, ArrayList<String> permissions) {
    this.name = name;
    this.prefix = prefix;
    this.sortId = sortId;
    this.permissions = permissions;
  }

  public String getName() {
    return name;
  }

  public String getPrefix() {
    return prefix;
  }

  public int getSortId() {
    return sortId;
  }

  public void setPrefix(String prefix) {
    this.prefix = prefix;
  }

  public void setSortId(int sortId) {
    this.sortId = sortId;
  }

  public List<String> getPermissions() {
    return permissions;
  }

  public void addPermission(String permission) {
    permissions.add(permission);
  }

  public void removePermission(String permission) {
    permissions.remove(permission);
  }

  public Document toDocument() {
    return new Document(Constants.GROUP_NAME_DOCUMENT_KEY, name)
      .append(Constants.GROUP_PREFIX_DOCUMENT_KEY, prefix)
      .append(Constants.GROUP_ID_DOCUMENT_KEY, sortId)
      .append(Constants.GROUP_PERMISSIONS_DOCUMENT_KEY, permissions);
  }

  public static Group create(String name, String prefix, int sortId) {
    Preconditions.checkNotNull(name);
    Preconditions.checkNotNull(prefix);
    return new Group(name, prefix, sortId);
  }

  public static Group of(Document document) {
    Preconditions.checkNotNull(document);
    String name = document.getString(Constants.GROUP_NAME_DOCUMENT_KEY);
    String prefix = document.getString(Constants.GROUP_PREFIX_DOCUMENT_KEY);
    int sortId = document.getInteger(Constants.GROUP_ID_DOCUMENT_KEY);
    ArrayList<String> permissions = document.get(Constants.GROUP_PERMISSIONS_DOCUMENT_KEY, ArrayList.class);
    return new Group(name, prefix, sortId, permissions);
  }
}
