package de.chunkworld.permission.group;

import com.google.common.base.Preconditions;
import com.mongodb.async.SingleResultCallback;
import com.mongodb.async.client.MongoCollection;
import com.mongodb.client.model.Filters;
import de.chunkworld.permission.Constants;
import de.chunkworld.permission.database.collection.DataCollection;
import de.chunkworld.permission.database.repository.GroupRepository;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

public final class GroupLoader {
  private final MongoCollection<Document> groupCollection;

  private GroupLoader(DataCollection groupCollection) {
    this.groupCollection = groupCollection.getCollection();
  }

  public CompletableFuture<Group> loadGroup(String groupName) {
    CompletableFuture<Group> completable = new CompletableFuture<>();

    Bson findGroupFilter = Filters.eq(Constants.GROUP_NAME_DOCUMENT_KEY, groupName);

    groupCollection.find(findGroupFilter).first(new SingleResultCallback<Document>() {
      @Override
      public void onResult(Document document, Throwable throwable) {
        if (throwable == null) {
          Group group = Group.of(document);
          completable.complete(group);
        } else {
          completable.completeExceptionally(throwable);
        }
      }
    });

    return completable;
  }

  public CompletableFuture<Map<String, Group>> loadAllGroups() {
    CompletableFuture<Map<String, Group>> completable = new CompletableFuture<>();
    Map<String, Group> groups = new HashMap<>();

    groupCollection.find().forEach(document -> {
      Group group = Group.of(document);
      groups.put(group.getName().toLowerCase(), group);
    }, new SingleResultCallback<Void>() {
      @Override
      public void onResult(Void aVoid, Throwable throwable) {
        if (throwable == null) {
          completable.complete(groups);
        } else {
          completable.completeExceptionally(throwable);
        }
      }
    });

    return completable;
  }

  public static GroupLoader create(DataCollection groupCollection) {
    Preconditions.checkNotNull(groupCollection);
    return new GroupLoader(groupCollection);
  }
}
