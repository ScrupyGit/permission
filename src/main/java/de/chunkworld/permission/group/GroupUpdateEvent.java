package de.chunkworld.permission.group;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class GroupUpdateEvent extends Event {
  private static final HandlerList HANDLER_LIST = new HandlerList();

  private GroupUpdateEvent() {}

  @Override
  public HandlerList getHandlers() {
    return HANDLER_LIST;
  }

  public static HandlerList getHandlerList() {
    return HANDLER_LIST;
  }

  public static GroupUpdateEvent create() {
    return new GroupUpdateEvent();
  }
}
