package de.chunkworld.permission;

public class Constants {
  public static final String GROUP_NAME_DOCUMENT_KEY = "name";
  public static final String GROUP_PREFIX_DOCUMENT_KEY = "prefix";
  public static final String GROUP_ID_DOCUMENT_KEY = "sortId";
  public static final String GROUP_PERMISSIONS_DOCUMENT_KEY = "permissions";
  public static final String PLAYER_UUID_DOCUMENT_KEY = "uuid";
  public static final String PLAYER_GROUP_DOCUMENT_KEY = "groupName";
  public static final String PLAYER_NAME_DOCUMENT_KEY = "playerName";
  public static final String DEFAULT_GROUP_NAME = "default";

  public static final String GROUP_NOT_EXISTS_MESSAGE_KEY = "groupNotExists";
  public static final String GROUP_ALREADY_EXISTS_MESSAGE_KEY = "groupAlreadyExists";
  public static final String GROUP_ADD_FAILURE_MESSAGE_KEY = "groupAddFailure";
  public static final String GROUP_ADDED_MESSAGE_KEY = "groupAddedSuccessfully";
  public static final String GROUP_DELETE_FAILURE_MESSAGE_KEY = "groupDeleteFailure";
  public static final String GROUP_DELETED_MESSAGE_KEY = "groupDeleted";
  public static final String NOT_ENOUGH_PERMISSION = "notEnoughPermission";
  public static final String PERMISSION_SUCCESSFULLY_ADDED_MESSAGE_KEY = "permissionSuccessfullyAdded";
}
