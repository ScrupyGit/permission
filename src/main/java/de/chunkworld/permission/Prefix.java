package de.chunkworld.permission;

import com.google.common.base.Preconditions;

public final class Prefix {
  private final String prefix;

  private Prefix(String prefix) {
    this.prefix = prefix;
  }

  public String replace(String messageToReplace, String replaceCode) {
    return messageToReplace.replaceAll(replaceCode, prefix);
  }

  public String apply(String message) {
    return prefix + message;
  }

  public static Prefix of(String prefix) {
    Preconditions.checkNotNull(prefix);
    String prefixReplacedColorCodes = prefix.replaceAll("&", "§");
    return new Prefix(prefixReplacedColorCodes);
  }
}
