package de.chunkworld.permission.config;

import com.google.common.base.Preconditions;
import de.chunkworld.permission.Prefix;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.HashMap;

public final class MessageConfig {
  private final HashMap<String, String> messages;
  private final FileConfiguration configFile;
  private final Prefix prefix;

  private MessageConfig(FileConfiguration configFile, Prefix prefix) {
    this.messages = new HashMap<>();
    this.configFile = configFile;
    this.prefix = prefix;
  }

  public String messageFromKey(String key) {
    return messages.computeIfAbsent(key,
      messageNotLoaded -> loadMessage(key)
    );
  }

  private String loadMessage(String key) {
    String messagePath = "messages." + key;
    String configMessage = configFile.getString(messagePath);
    String chatMessage = transformToChatMessage(configMessage);
    messages.put(key, chatMessage);
    return chatMessage;
  }

  private String transformToChatMessage(String message) {
    String messageWithReplacedColorCodes = replaceColorCodes(message);
    return replacePrefix(messageWithReplacedColorCodes);
  }

  private String replaceColorCodes(String message) {
    return message.replaceAll("&", "§");
  }

  private String replacePrefix(String message) {
    String prefixReplaceCode = "%prefix";
    return prefix.replace(message, prefixReplaceCode);
  }

  public static MessageConfig create(FileConfiguration configFile, Prefix prefix) {
    Preconditions.checkNotNull(configFile);
    Preconditions.checkNotNull(prefix);
    return new MessageConfig(configFile, prefix);
  }
}
