package de.chunkworld.permission.command.permission;

import com.google.common.base.Preconditions;
import de.chunkworld.permission.Constants;
import de.chunkworld.permission.command.SubCommand;
import de.chunkworld.permission.config.MessageConfig;
import de.chunkworld.permission.database.repository.GroupRepository;
import org.bukkit.command.CommandSender;

import static de.chunkworld.permission.Constants.GROUP_NOT_EXISTS_MESSAGE_KEY;

public final class AddPermissionSubCommand implements SubCommand {
  private final GroupRepository groupRepository;
  private final MessageConfig messageConfig;

  private AddPermissionSubCommand(GroupRepository groupRepository, MessageConfig messageConfig) {
    this.groupRepository = groupRepository;
    this.messageConfig = messageConfig;
  }

  @Override
  public void execute(CommandSender sender, String[] arguments) {
    String groupName = arguments[1].toLowerCase();
    String permission = arguments[2];

    if (!groupRepository.groupExists(groupName)) {
      sender.sendMessage(messageConfig.messageFromKey(GROUP_NOT_EXISTS_MESSAGE_KEY));
      return;
    }

    groupRepository.addPermission(groupName, permission)
      .thenAccept(success -> {
        sender.sendMessage(String.format(messageConfig.messageFromKey(Constants.PERMISSION_SUCCESSFULLY_ADDED_MESSAGE_KEY), groupName, permission));
      })
      .exceptionally(failure -> {
        sender.sendMessage("Failed to add permission");
        return null;
      });
  }

  public static AddPermissionSubCommand create(GroupRepository groupRepository, MessageConfig messageConfig) {
    Preconditions.checkNotNull(groupRepository);
    Preconditions.checkNotNull(messageConfig);
    return new AddPermissionSubCommand(groupRepository, messageConfig);
  }
}
