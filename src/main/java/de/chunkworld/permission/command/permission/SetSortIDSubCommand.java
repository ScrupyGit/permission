package de.chunkworld.permission.command.permission;

import com.google.common.base.Preconditions;
import de.chunkworld.permission.command.SubCommand;
import de.chunkworld.permission.config.MessageConfig;
import de.chunkworld.permission.database.repository.GroupRepository;
import org.bukkit.command.CommandSender;

public class SetSortIDSubCommand implements SubCommand {
  private final GroupRepository groupRepository;
  private final MessageConfig messageConfig;

  private SetSortIDSubCommand(GroupRepository groupRepository, MessageConfig messageConfig) {
    this.groupRepository = groupRepository;
    this.messageConfig = messageConfig;
  }

  @Override
  public void execute(CommandSender sender, String[] arguments) {
    String groupName = arguments[1].toLowerCase();
    int id = Integer.parseInt(arguments[2]);

    if (!groupRepository.groupExists(groupName)) {
      sender.sendMessage("This group doent exists.");
      return;
    }

    groupRepository.setSortId(groupName, id)
      .thenAccept(success -> {
        sender.sendMessage("Successfully changed id.");
      })
      .exceptionally(failure -> {
        sender.sendMessage("Failed to change id.");
        return null;
      });
  }

  public static SetSortIDSubCommand create(GroupRepository groupRepository, MessageConfig messageConfig) {
    Preconditions.checkNotNull(groupRepository);
    Preconditions.checkNotNull(messageConfig);
    return new SetSortIDSubCommand(groupRepository, messageConfig);
  }
}
