package de.chunkworld.permission.command.permission;

import com.google.common.base.Preconditions;
import de.chunkworld.permission.Constants;
import de.chunkworld.permission.command.SubCommand;
import de.chunkworld.permission.config.MessageConfig;
import de.chunkworld.permission.database.repository.GroupRepository;
import org.bukkit.command.CommandSender;

import static de.chunkworld.permission.Constants.*;

public final class RemoveGroupSubCommand implements SubCommand {
  private final GroupRepository groupRepository;
  private final MessageConfig messageConfig;

  private RemoveGroupSubCommand(GroupRepository groupRepository, MessageConfig messageConfig) {
    this.groupRepository = groupRepository;
    this.messageConfig = messageConfig;
  }

  @Override
  public void execute(CommandSender sender, String[] arguments) {
    String groupName = arguments[1];
    if (!groupRepository.groupExists(groupName)) {
      sender.sendMessage(messageConfig.messageFromKey(GROUP_NOT_EXISTS_MESSAGE_KEY));
    } else {
      groupRepository.removeGroup(groupName)
        .thenAccept(success -> {
          String groupDeletedMessage = String.format(messageConfig.messageFromKey(GROUP_DELETED_MESSAGE_KEY), groupName);
          sender.sendMessage(groupDeletedMessage);
        })
        .exceptionally(failure -> {
          sender.sendMessage(messageConfig.messageFromKey(GROUP_DELETE_FAILURE_MESSAGE_KEY));
          return null;
        });
    }
  }

  public static RemoveGroupSubCommand create(GroupRepository groupRepository, MessageConfig messageConfig) {
    Preconditions.checkNotNull(groupRepository);
    Preconditions.checkNotNull(messageConfig);
    return new RemoveGroupSubCommand(groupRepository, messageConfig);
  }
}
