package de.chunkworld.permission.command.permission;

import de.chunkworld.permission.command.BaseCommand;
import de.chunkworld.permission.command.SubCommand;
import de.chunkworld.permission.config.MessageConfig;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import static de.chunkworld.permission.Constants.NOT_ENOUGH_PERMISSION;

public class PermissionBaseCommand extends BaseCommand {
  private final MessageConfig messageConfig;

  private PermissionBaseCommand(MessageConfig messageConfig) {
    this.messageConfig = messageConfig;
  }

  @Override
  public boolean onCommand(
    CommandSender sender,
    Command command,
    String label,
    String[] arguments
  ) {
    if (!sender.hasPermission("permission.*")) {
      sender.sendMessage(messageConfig.messageFromKey(NOT_ENOUGH_PERMISSION));
      return true;
    }
    if (arguments.length <= 0) {
      sendHelp(sender);
      return true;
    }
    String commandArgument = arguments[0].toLowerCase();
    SubCommand subCommand = subCommands.get(commandArgument);
    if (subCommand == null) {
      sendHelp(sender);
      return true;
    }
    subCommand.execute(sender, arguments);
    return false;
  }

  private void sendHelp(CommandSender sender) {
    sender.sendMessage("Here is the help for all Commands");
  }

  public static PermissionBaseCommand create(MessageConfig messageConfig) {
    return new PermissionBaseCommand(messageConfig);
  }
}
