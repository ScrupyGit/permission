package de.chunkworld.permission.command.permission;

import com.google.common.base.Preconditions;
import de.chunkworld.permission.command.SubCommand;
import de.chunkworld.permission.config.MessageConfig;
import de.chunkworld.permission.database.repository.GroupRepository;
import org.bukkit.command.CommandSender;

import static de.chunkworld.permission.Constants.*;

public final class AddGroupSubCommand implements SubCommand {
  private final GroupRepository groupRepository;
  private final MessageConfig messageConfig;

  private AddGroupSubCommand(GroupRepository groupRepository, MessageConfig messageConfig) {
    this.groupRepository = groupRepository;
    this.messageConfig = messageConfig;
  }

  @Override
  public void execute(CommandSender sender, String[] arguments) {
    String groupName = arguments[1];
    if (groupRepository.groupExists(groupName)) {
      sender.sendMessage(messageConfig.messageFromKey(GROUP_ALREADY_EXISTS_MESSAGE_KEY));
    } else {
      addGroup(arguments, sender);
    }
  }

  private void addGroup(String[] arguments, CommandSender sender) {
    String groupName = arguments[1];
    int sortId = Integer.parseInt(arguments[2]);
    String prefix = stringOfArguments(arguments, 3, arguments.length);

    groupRepository.addGroup(groupName, prefix, sortId)
      .thenAccept(group -> {
        String groupAddedMessage = String.format(messageConfig.messageFromKey(GROUP_ADDED_MESSAGE_KEY), group.getName());
        sender.sendMessage(groupAddedMessage);
      })
      .exceptionally(error -> {
        sender.sendMessage(messageConfig.messageFromKey(GROUP_ADD_FAILURE_MESSAGE_KEY));
        return null;
      });
  }

  private String stringOfArguments(String[] arguments, int start, int end) {
    StringBuilder prefix = new StringBuilder();
    for (int i = start; i < end; i++) {
      if (i != start) {
        prefix.append(" ");
      }
      prefix.append(arguments[i]);
    }
    return prefix.toString();
  }

  public static AddGroupSubCommand create(GroupRepository groupRepository, MessageConfig messageConfig) {
    Preconditions.checkNotNull(groupRepository);
    Preconditions.checkNotNull(messageConfig);
    return new AddGroupSubCommand(groupRepository, messageConfig);
  }
}
