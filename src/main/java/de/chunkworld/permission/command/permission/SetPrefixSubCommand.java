package de.chunkworld.permission.command.permission;

import com.google.common.base.Preconditions;
import de.chunkworld.permission.command.SubCommand;
import de.chunkworld.permission.config.MessageConfig;
import de.chunkworld.permission.database.repository.GroupRepository;
import org.bukkit.command.CommandSender;

public class SetPrefixSubCommand implements SubCommand {
  private final GroupRepository groupRepository;
  private final MessageConfig messageConfig;

  private SetPrefixSubCommand(GroupRepository groupRepository, MessageConfig messageConfig) {
    this.groupRepository = groupRepository;
    this.messageConfig = messageConfig;
  }

  @Override
  public void execute(CommandSender sender, String[] arguments) {
    String groupName = arguments[1].toLowerCase();
    String prefix = stringOfArguments(arguments, 2, arguments.length);

    if (!groupRepository.groupExists(groupName)) {
      sender.sendMessage("This group doent exists.");
      return;
    }

    groupRepository.setPrefix(groupName, prefix)
      .thenAccept(success -> {
        sender.sendMessage("Successfully changed prefix.");
      })
      .exceptionally(failure -> {
        sender.sendMessage("Failed to change prefix.");
        return null;
      });
  }

  private String stringOfArguments(String[] arguments, int start, int end) {
    StringBuilder prefix = new StringBuilder();
    for (int i = start; i < end; i++) {
      if (i != start) {
        prefix.append(" ");
      }
      prefix.append(arguments[i]);
    }
    return prefix.toString();
  }

  public static SetPrefixSubCommand create(GroupRepository groupRepository, MessageConfig messageConfig) {
    Preconditions.checkNotNull(groupRepository);
    Preconditions.checkNotNull(messageConfig);
    return new SetPrefixSubCommand(groupRepository, messageConfig);
  }
}
