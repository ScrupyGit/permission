package de.chunkworld.permission.command.permission;

import com.google.common.base.Preconditions;
import de.chunkworld.permission.command.SubCommand;
import de.chunkworld.permission.config.MessageConfig;
import de.chunkworld.permission.database.repository.GroupRepository;
import de.chunkworld.permission.database.repository.PlayerRepository;
import de.chunkworld.permission.group.Group;
import de.chunkworld.permission.player.PlayerGroupUpdateEvent;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;

public class SetGroupSubCommand implements SubCommand {
  private final GroupRepository groupRepository;
  private final PlayerRepository playerRepository;
  private final MessageConfig messageConfig;

  private SetGroupSubCommand(GroupRepository groupRepository, PlayerRepository playerRepository, MessageConfig messageConfig) {
    this.groupRepository = groupRepository;
    this.playerRepository = playerRepository;
    this.messageConfig = messageConfig;
  }

  @Override
  public void execute(CommandSender sender, String[] arguments) {
    String playerName = arguments[1].toLowerCase();
    String groupName = arguments[2].toLowerCase();
    Group group = groupRepository.getGroupByName(groupName);
    String playerUUID = Bukkit.getPlayer(playerName).getUniqueId().toString();
    playerRepository.setGroup(playerUUID, group).thenAccept(success -> {
      sender.sendMessage("Rank neu");
    }).exceptionally(failure -> {
      sender.sendMessage("fehler");
      return null;
    });
  }

  public static SetGroupSubCommand create(GroupRepository groupRepository, PlayerRepository playerRepository, MessageConfig messageConfig) {
    Preconditions.checkNotNull(groupRepository);
    Preconditions.checkNotNull(playerRepository);
    Preconditions.checkNotNull(messageConfig);
    return new SetGroupSubCommand(groupRepository, playerRepository, messageConfig);
  }
}
