package de.chunkworld.permission.command.permission;

import com.google.common.base.Preconditions;
import de.chunkworld.permission.command.SubCommand;
import de.chunkworld.permission.config.MessageConfig;
import de.chunkworld.permission.database.repository.GroupRepository;
import de.chunkworld.permission.group.Group;
import org.bukkit.command.CommandSender;

public final class RemovePermissionSubCommand implements SubCommand {
  private final GroupRepository groupRepository;
  private final MessageConfig messageConfig;

  private RemovePermissionSubCommand(GroupRepository groupRepository, MessageConfig messageConfig) {
    this.groupRepository = groupRepository;
    this.messageConfig = messageConfig;
  }

  @Override
  public void execute(CommandSender sender, String[] arguments) {
    String groupName = arguments[1].toLowerCase();
    String permissionToRemove = arguments[2];

    if (!groupRepository.groupExists(groupName)) {
      sender.sendMessage("This group dont exists");
      return;
    }

    Group group = groupRepository.getGroupByName(groupName);

    if (!group.getPermissions().contains(permissionToRemove)) {
      sender.sendMessage("This group dont have this permission");
      return;
    }

    groupRepository.removePermission(groupName, permissionToRemove)
      .thenAccept(success -> {
        sender.sendMessage("Successfully removed permission");
      })
      .exceptionally(failure -> {
        sender.sendMessage("Failed to removed permission");
        return null;
      });
  }

  public static RemovePermissionSubCommand create(GroupRepository groupRepository, MessageConfig messageConfig) {
    Preconditions.checkNotNull(groupRepository);
    Preconditions.checkNotNull(messageConfig);
    return new RemovePermissionSubCommand(groupRepository, messageConfig);
  }
}
