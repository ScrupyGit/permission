package de.chunkworld.permission.command;

import org.bukkit.command.CommandSender;

public interface SubCommand {
  void execute(CommandSender sender, String[] arguments);
}
