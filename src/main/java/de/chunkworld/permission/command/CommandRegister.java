package de.chunkworld.permission.command;

import com.google.common.base.Preconditions;
import de.chunkworld.permission.Permission;
import de.chunkworld.permission.command.permission.*;
import de.chunkworld.permission.config.MessageConfig;
import de.chunkworld.permission.database.repository.GroupRepository;
import de.chunkworld.permission.database.repository.PlayerRepository;

public final class CommandRegister {
  private final Permission plugin;
  private final GroupRepository groupRepository;
  private final MessageConfig messageConfig;
  private final PlayerRepository playerRepository;

  private CommandRegister(
    Permission plugin,
    GroupRepository groupRepository,
    MessageConfig messageConfig,
    PlayerRepository playerRepository
  ) {
    this.plugin = plugin;
    this.groupRepository = groupRepository;
    this.messageConfig = messageConfig;
    this.playerRepository = playerRepository;
    registerCommands();
  }

  private void registerCommands() {
    BaseCommand permissionCommand = PermissionBaseCommand.create(messageConfig);
    permissionCommand.registerSubCommand("addGroup", AddGroupSubCommand.create(groupRepository, messageConfig));
    permissionCommand.registerSubCommand("removeGroup", RemoveGroupSubCommand.create(groupRepository, messageConfig));
    permissionCommand.registerSubCommand("setPrefix", SetPrefixSubCommand.create(groupRepository, messageConfig));
    permissionCommand.registerSubCommand("setId", SetSortIDSubCommand.create(groupRepository, messageConfig));
    permissionCommand.registerSubCommand("addPermission", AddPermissionSubCommand.create(groupRepository, messageConfig));
    permissionCommand.registerSubCommand("removePermission", RemovePermissionSubCommand.create(groupRepository, messageConfig));
    permissionCommand.registerSubCommand("setGroup", SetGroupSubCommand.create(groupRepository, playerRepository, messageConfig));

    plugin.getCommand("permission").setExecutor(permissionCommand);
  }

  public static CommandRegister create(
    Permission plugin,
    GroupRepository groupRepository,
    MessageConfig messageConfig,
    PlayerRepository playerRepository
  ) {
    Preconditions.checkNotNull(plugin);
    Preconditions.checkNotNull(groupRepository);
    Preconditions.checkNotNull(messageConfig);
    Preconditions.checkNotNull(playerRepository);
    return new CommandRegister(plugin, groupRepository, messageConfig, playerRepository);
  }
}
