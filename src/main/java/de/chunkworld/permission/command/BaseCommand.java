package de.chunkworld.permission.command;

import org.bukkit.command.CommandExecutor;

import java.util.HashMap;
import java.util.Map;

public abstract class BaseCommand implements CommandExecutor {
  public Map<String, SubCommand> subCommands;

  public BaseCommand() {
    this.subCommands = new HashMap<>();
  }

  public void registerSubCommand(String subCommandName, SubCommand subCommand) {
    subCommands.put(subCommandName.toLowerCase(), subCommand);
  }
}
